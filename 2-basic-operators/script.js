const currentYear = 2037
const ageJane = currentYear - 1991;
const ageSarah = currentYear - 2020;
console.log(ageJane, ageSarah);

// Math operators

console.log(ageJane * 2, ageJane / 2, 2 ** 3);
// 2 ** 3 means 2 to the power of 3 = 2 * 2 * 2

const firstName = 'Jane'
const lastName = 'Zdravevski'
console.log(firstName + ' ' + lastName)

console.log(typeof(firstName)) // typeof() is also an operator

// Assignment Operators
let x = 10 + 5 // We got 2 operators here. We got the plus and we got the equals (which is assignment operator)
console.log(x)
x += 10; // x = x + 10;
console.log(x)
x -= 24; // x = x - 25;
console.log(x)
x *= 4; // same for /=
console.log(x)
x++; // x = x + 1
console.log(x)

// Comparison operators
console.log(ageJane > ageSarah); // >, <, >=, <=
console.log(ageSarah >= 18);
const isFullAge = ageSarah >= 18;
console.log(currentYear - 1991 > currentYear - 2018)


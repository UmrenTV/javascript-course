let age = 30;
age = 32;


// const birthYear = 1988;
console.log(age)

// const job; - this won't work. Const need initial value ALWAYS, since you can't change it later.

var job = 'coder'
console.log(job)
job = "programmer"
console.log(job)

lastName = 'Zdravevski' // This is a bad practice since it will create a variable in the global object, which is not optimal.
console.log(lastName)

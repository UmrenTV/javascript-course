// Brief clearance on the start: Conversion is whenever we decide to convert one type to another.
// Coersion is whenever JavaScript decides to do that for us. We have seen couple times
// coersion in action  like when you try this:
console.log(2 + 2 + 'John' + 2 + 2);
// and we see 4John22. First it does the adition, then it does the cohersion of the number
// 4 to string, and then it transoforms everything else after that to string, and concatonates.

// Lets try conversion:

const inputYear = '1991';
console.log(Number(inputYear), inputYear); // Note in the console the different types!
const inputYearNumber = Number(inputYear) // We saved it here as another var as Number
console.log(inputYear + 18); // Here we try addition with String value and it coerces the number 18 to string, so it can concatonate
console.log(inputYearNumber + 18); // This is the actual addition - number to number. String to number won't work.

console.log(Number('Jane')) // Since this is not realy a string that can be converted to number
// in the console we are getting a value called "NaN" which means Not a Number. And the interesting
// part about NaN is that it's type of Number, which means "I am an invalid number". To Check:
console.log(typeof NaN); // this returns number.
console.log(typeof 1); // same as this.
// so TLDR, NaN is a type of number, that contains invalid number.

console.log(String(123), 123); // here is to conver to String. Notice the color of the numbers.
// There is also a conversion to Boolean, but that comes later, with the Truthy / Falsy lesson,
// because Boolean() has some special behavior that needs more drilling into.

// Lets try coersion:

console.log("I am " + 23 + " years old");
console.log("I am " + '23' + " years old"); // These 2 logs are the same, because Javascript
// decides here to coerce the number into string, so it completes the string.

console.log('23' + '10' + 3); // This coerces into strings.
console.log('23' - '10' - 3); // the - sign actually makes the coercion to go other way around
// by converting the strings into numbers. So we get output: 23103 and 10.
console.log('23' * '2') // This will return 46 because the * and / operators can only work that
// way, as mathematical operators.
console.log('john' * 2); // This returns NaN. "John" is not a number, and there is no way to
// multiply with a string, so it has no idea what to do :D

console.log('23' > '18'); // This also converts the strings into numbers, if it's possible.
// The only operator that does that other way around, is the +, which prefers strings.

console.log('john' > '11'); // This, however, returns true. With < sign is false.
// I think we'll learn this later when we get to truthy / falsy values. No idea how john > 11 otherwise :D

let n = '1' + 1
n = n - 1;
console.log(n) // This returns 10.
// n = '1' + 1 (transformed to string) = '11'
// n = '11' (11 gets to number coz of the - operator) - 1 = 10 (as number)



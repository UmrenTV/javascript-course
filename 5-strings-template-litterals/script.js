const firstName = 'Jane';
const lastName = 'Zdravevski';
const job = 'coder';
const birthYear = 1988;
const now = 2021;

const jane = 'I\'m ' + firstName + ' ' + lastName + ', a ' + (now - birthYear) + ' years old ' + job + '.'

console.log(jane);

const janeNew = `I am ${firstName} ${lastName}, and I am ${now - birthYear} years old ${job}.`;
console.log('JaneNEW: ' + janeNew);

// So basically whenever you use `` instead of '' or "" you are writing into template mode. In that mode
// you can use template litterals, where you are allowed to add variables in the middle of the string, using
// the ${} syntax, and whenever the string is getting called, it will replace them with the values of the variables.
// Note: Many of the programers are even using template mode whenever they are writing normal strings, so
// it is easier to just put in a template litteral whenever is needed without thinking about the quotations.

console.log('Strong with \n 2 lines');
console.log(`Another string
with two lines`);

// in order to print out two lines, you need to use the \n in regular string to add a new line.
// if you are using template mode with `` you can just press enter, and it knows to put new line.

const age = 15;

if (age >= 18) {
console.log('Sara can start driving licence');
} else {
console.log(`Sara is too young. Wait another
${18 - age} years :)`)
}

// if () {
// } else {} - This structure is called if/else control structure.


const birthYear = 2001;
let century;

if (birthYear <= 2000 ){
century = 20;
} else {
century = 21}

console.log(`You are born in ${century} century.`)

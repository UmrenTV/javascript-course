// Challenge #1:
// Store mark weight and height from 2 examples and calculate BMI with the
// following formula: weight / height ** 2
// height ** 2 is same as height * height btw. It's exponent.

// Example 1:
const markWeight = 78;
const johnWeight = 92;
const markHeight = 1.69
const johnHeight = 1.95

const markBMI = markWeight / markHeight ** 2;
const johnBMI = johnWeight / johnHeight ** 2;

const markHigher = markBMI > johnBMI;

console.log('#1 Mark: ', markBMI)
console.log('#1 John: ',johnBMI);

// Examle 2:
// I should have done with another data set normal way, but I liked to try this
// advanced shenenigans with function. Easier, huh? :D
function bmiCalculator(name, weight, height) {
    console.log('#2 ' + name + ': ' + weight / height ** 2);
}

bmiCalculator('Mark', 95, 1.88);
bmiCalculator('John', 85, 1.76)

// Challenge #2:
// Use the data above to:
// 1. Pring a nice output to the console saying who has the higher BMI in
// format "Mark's BMI is higher than John's" or vice versa:
// 2. Use a temlate literal to include the BMI values in the outputs.
// Example: Mark's BMI (28.3) is higher than John's (23.9)!"

if (markHigher) {
console.log('#1 Mark BMI is higher than John\'s!')
} else {
console.log('#1 John BMI is higher than Mark\'s!')
}

const markWeightTwo = 95
const johnWeightTwo = 85
const markHeightTwo = 1.88
const johnHeightTwo = 1.76

const markBMItwo = markWeightTwo / markHeightTwo ** 2;
const johnBMItwo = johnWeightTwo / johnHeightTwo ** 2;

const markHigherTwo = markBMItwo > johnBMItwo
if (markHigherTwo) {
console.log(`#2 Mark BMI(${markBMItwo.toFixed(2)}) is higher than the BMI of John(${johnBMItwo.toFixed(2)})!`)
} else {
console.log(`#2 John BMI(${johnBMItwo.toFixed(2)}) is higher than the BMI of Mark(${markBMItwo.toFixed(2)})!`)
}

// the toFixed trick is something that uncle Google helped me with coz the BMI result had
// like 10 decimal points. toFixed is allowing you to transform it to float with () decimals.


